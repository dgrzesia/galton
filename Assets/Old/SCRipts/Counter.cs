﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Counter : MonoBehaviour {

	public int count = 0;
    public Text text;

	void OnTriggerEnter(Collider other)
	{
        if (other.tag == "Ball")
        {
            count++;
            text.text = count.ToString();
        }
		

	}
}
