﻿using UnityEngine;
using System.Collections;

public class Pins : MonoBehaviour {

	public GameObject pin;

	void Awake()
	{
		Camera.main.transform.position = new Vector3(Manager.instance.xpos + Manager.instance.width, Manager.instance.height * 3 / 2, -(Manager.instance.width + Manager.instance.height) -5f);
		for (int i = 0; i < Manager.instance.height; i++)
		{
			for (int j = 0; j < Manager.instance.width; j++)
			{
				GameObject clone = (GameObject)Instantiate(pin);
				clone.transform.SetParent(transform);
				clone.transform.position = new Vector3((Manager.instance.xspacing * 2f * j) + Manager.instance.xpos + i%2, 2 * i, 10.0f);
			}
		}
	}
}
