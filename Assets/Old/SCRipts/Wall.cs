﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public GameObject wall;
	public GameObject surface;

	void Awake()
	{
		for (float i = -1; i < Manager.instance.width + 1; i++)
		{
			GameObject cloneWall = (GameObject)Instantiate(wall);
			cloneWall.transform.SetParent(transform);
			cloneWall.transform.position = new Vector3((Manager.instance.xspacing * 2f * i) + Manager.instance.xpos, -3.85f, 10.0f);
			GameObject cloneSurf = (GameObject)Instantiate(surface);
			cloneSurf.transform.SetParent(transform);
			cloneSurf.transform.position = new Vector3((Manager.instance.xspacing * 2f * i) + Manager.instance.xpos + 1.0f, -6f, 10.0f);
			cloneSurf.AddComponent<Counter>();
		}
	}
}
