﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour {

	protected static Manager _instance;
	public static Manager instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = (Manager)FindObjectOfType(typeof(Manager));
			}
			return _instance;
		}
	}

	[Range(1, 10000)]
	public int width = 13;
	[Range(1, 10000)]
	public int height = 10;
	public float xpos = 20.5f;
	public float xspacing = 1;
}
