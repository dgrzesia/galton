﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	float timeSinceLastSphere;
	float timeofLastSphere;
	public GameObject ball;

    void Start()
    {
        GameObject cloneSphere;

        cloneSphere = (GameObject)Instantiate(ball);
        cloneSphere.transform.SetParent(transform);
        cloneSphere.transform.position = new Vector3((Manager.instance.xspacing * 2f * (Manager.instance.width / 2f - 0.5f)) + Manager.instance.xpos + Random.Range(-0.1f, 0.1f), 3.0f * Manager.instance.height, 10.0f);

        timeofLastSphere = Time.time;
    }

	void Update()
	{
		timeSinceLastSphere =  -timeofLastSphere + Time.time;

		if (timeSinceLastSphere > 3.0f)
		{

			GameObject cloneSphere;

			cloneSphere = (GameObject)Instantiate(ball);
			cloneSphere.transform.SetParent(transform);
			cloneSphere.transform.position = new Vector3((Manager.instance.xspacing * 2f * (Manager.instance.width / 2f - 0.5f)) + Manager.instance.xpos + Random.Range(-0.1f, 0.1f), 3.0f * Manager.instance.height, 10.0f);

			timeofLastSphere = Time.time;

		}
	}
}
