using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BoardController : MonoBehaviour {

	public int holeCount;
	public Transform wallPrefab;
	public float wallWidth;
	public float ballSize;
	public Transform pinPrefab;
	public Transform ballPrefab;
    public Text holeText;
    public int ballsNumber = 10;
    int counter = 0;
    public Text ballsCounter;
	private List<GameObject> walls;
	private List<GameObject> pins;
	private List<GameObject> balls;
    private List<GameObject> cols;

    public List<Text> text = new List<Text>();
    public GameObject col;

    int wallCount;
    float positionInterval;
	
	public float spawningInterval;
    float secondsFromLastSpawning;

    public BarChart _BarChart = null;
    public LineChart _LineChart = null;
    float[] data;

    void Start () {
        foreach (var t in text)
        {
            t.enabled = false;
        }
        Init ();
	}
	
	void Update () {
		
		if (Input.GetKeyUp ("r")) {
			Reset ();
		}
		
		if (Input.GetKeyUp ("left")) {
			if (holeCount > 13) {
				holeCount --;
				Reset ();
			}
		}
		
		if (Input.GetKeyUp ("right")) {
            if (holeCount < 25)
            {
                holeCount++;
                Reset();
            }
			
		}
		
		secondsFromLastSpawning += Time.deltaTime;
		while (secondsFromLastSpawning > spawningInterval) {
			InstantiateBall();
			secondsFromLastSpawning -= spawningInterval;
		}

        for (int i = 0; i < cols.Count; i++)
        {
                data[i] = cols[i].GetComponent<Counter>().count;
        }

        if (_BarChart != null)
        {
            _BarChart.UpdateData(data);
        }
        if (_LineChart != null)
        {
            _LineChart.UpdateData(data);
        }
    }
	
	private void Reset() {
		foreach (var wall in walls) {
			Destroy (wall);
		}
		foreach (var pin in pins) {
			Destroy (pin);
		}
		foreach (var ball in balls) {
			Destroy (ball);
		}
        foreach (var c in cols)
        {
            Destroy(c);
        }
        foreach (var t in text)
        {
            t.enabled = false;
        }
		Init ();
	}
	
	private void Init() {
        data = new float[holeCount];
        counter = 0;
        ballsCounter.text = "Ball 0/" + ballsCounter.ToString();
		wallCount = holeCount - 1;
		secondsFromLastSpawning = 0f;
		wallWidth = 6f / wallCount;
		InstantiateObjects();
		balls = new List<GameObject>();
        holeText.text = "Holes: " + holeCount.ToString();
	}	
	
	private void InstantiateBall() {
        if (counter < ballsNumber)
        {
            var ball = ((Transform)Instantiate(ballPrefab)).gameObject;
            ball.transform.localPosition = new Vector3((positionInterval - ballSize) * (Random.value - 0.5f), 13f, 1f);
            ball.transform.localScale = new Vector3(ballSize, ballSize, ballSize);
            balls.Add(ball);
            counter++;
            ballsCounter.text = "Ball " + counter.ToString() + "/" + ballsNumber.ToString();
        }
		
	}
	
	private void InstantiateObjects() {
		var spaceForHole = (19f - wallWidth * wallCount) / holeCount;
		ballSize = spaceForHole * 0.82f;
		positionInterval = spaceForHole + wallWidth;
		var xOffset = 9.5f - spaceForHole - wallWidth / 2f;
		InstantiateWalls(xOffset);
		InstantiatePins(xOffset);
	}
	
	private void InstantiateWalls(float xOffset) {
		walls = new List<GameObject>();
        cols = new List<GameObject>();
		var wallCount = holeCount - 1;
        for (var i = 0; i < wallCount; ++i) {
			var x = xOffset - positionInterval * i;
			var wall = ((Transform) Instantiate(wallPrefab)).gameObject;
			wall.transform.localPosition = new Vector3(x, -10f, 1f);
			wall.transform.localScale = new Vector3(wallWidth, 9f, 1f);
			walls.Add(wall);
        }

        for (int i = 0; i < holeCount; i++)
        {
            var x = xOffset - positionInterval * i + wallWidth;
            GameObject go = Instantiate(col, new Vector3(x, -10f, 1f), Quaternion.identity) as GameObject;
            go.GetComponent<BoxCollider>().size = new Vector3(wallWidth, go.GetComponent<BoxCollider>().size.y, go.GetComponent<BoxCollider>().size.z);
            cols.Add(go);
            text[i].enabled = true;
            text[i].text = "0";
            go.GetComponent<Counter>().text = text[i];
        }
        
    }
	
	private void InstantiatePins(float xOffset) {
		pins = new List<GameObject>();
		var pinCount = holeCount - 2;
		var yInterval = Mathf.Sin (Mathf.PI / 3f) * positionInterval;
		var yOffset = -5.5f;
		for (var line = 0; line < pinCount + 2; ++line) {
			var xOffsetOfCurrentLine = xOffset - positionInterval * ((line % 2) * 0.5f);
			var y = yOffset + yInterval * line;
			var pinCountOfCurrentLine = pinCount + ((line + 1) % 2);
			for (var i = 0; i < pinCountOfCurrentLine; ++i) {
				var x = xOffsetOfCurrentLine - positionInterval * i;
				var pin = ((Transform) Instantiate(pinPrefab)).gameObject;
				pin.transform.localPosition = new Vector3(x, y, 1f);
				pins.Add(pin);
				
			}
		}
	}
}
